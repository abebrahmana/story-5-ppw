from django.shortcuts import render
from django.http import HttpResponse
from .forms import FriendForm
from .models import Friend


# Create your views here.
def index(request):
    return render(request, "app1/index.html")

def blog(request):
    return render(request, "app1/blog.html")

def about(request):
    return render(request, "app1/about.html")

def passion(request):
    return render(request, "app1/passion.html")

def work(request):
    return render(request, "app1/work.html")

def modal(request):
    return render(request, "app1/modal.html")

def friends(request):
    allfriends = Friend.objects.all()
    context = {'allfriends': allfriends}
    return render(request, "app1/friends.html", context)

def forms(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            allfriends = Friend.objects.all()
            context = {'allfriends': allfriends}
            return render(request, 'app1/friends.html', context)
    else:
        form = FriendForm()
        return render(request, 'app1/form.html', {'form': form})
