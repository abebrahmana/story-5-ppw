from django.db import models
from django.conf import settings

# Create your models here.

class Year(models.Model):
    yearName = models.CharField(max_length=10)
    classYear = models.IntegerField(default=2019)
    def __str__(self):
        return "%s %s" % (self.yearName, self.classYear)

class Friend(models.Model):
    name = models.CharField(max_length=50)
    hobby = models.TextField()
    favourite = models.TextField()
    classYear = models.ForeignKey(Year, on_delete=models.CASCADE)
    def __str__ (self):
        return self.name

