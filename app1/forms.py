from django import forms
from django.forms import ModelForm
from .models import Year, Friend
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
